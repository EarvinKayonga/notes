package main

import "fmt"

const (
      mess string = "Hello";
)

var (
    message string
)

func	main(){
	var myFloat  float32 = 42.0
	message = "Hello";
	myString := "YLO"
	ar := [...]int{32, 32}
	ar[0] = 42
	fmt.Println(message)
	println(mess)
	println(myFloat)
	println(myString)
	fmt.Println(ar)
}